const parser = require('react-webgl-viewer/parser'),
    fs = require('fs'),
    path = require('path');

parser.parse('./binary/FAB.obj')
    .then(modelData => {
        let all = Promise.all(modelData.models.map(model => {

            // console.log(model);

            return new Promise((resolve, reject) => {
                fs.writeFile(
                    path.join(path.resolve('./models'), `${model.name}.json`),
                    model.stringify(),
                    reject
                );
                resolve();
            });
        }));
    })
    .catch(e => {
        console.error(e);
    });
