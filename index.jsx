import React from 'react';
import ReactDOM from 'react-dom';

import WGL from 'react-webgl-viewer';

import F from './models/F_Untitled.010.json';
import I1 from './models/I1_Untitled.008.json';
import N from './models/N_Untitled.007.json';
import E from './models/E_Untitled.005.json';

import A from './models/A_Untitled.002.json';
import R from './models/R_Untitled.011.json';
import T from './models/T_Untitled.003.json';

import B1 from './models/B1_Untitled.006.json';
import I2 from './models/I2_Untitled.012.json';
import B2 from './models/B2_Untitled.json';
import L from './models/L_Untitled.013.json';
import I3 from './models/I3_Untitled.001.json';

import Zoomer from './models/Zoomer_Untitled.004.json';
import Border from './models/Border_Untitled.009.json';

import './style.scss';

ReactDOM.render(<WGL
    loadModels={[
        F, I1, N, E,
        A, R, T,
        B1, I2, B2, L, I3,
        Zoomer,
        Border
    ]}
/>, document.getElementById('body'));
