const ExtractTextPlugin = require('extract-text-webpack-plugin'),
    webpack = require('webpack'),
    path = require('path')
    ;
let plugins = [];
module.exports = function (env) {
    return {
        entry: {
            bundle: path.resolve('./index.jsx')
        },
        watch: !env || !env.production,
        devtool: (!env || !env.production)
            && 'eval'
            // && 'cheap-module-source-map'
            || 'source-map',
        output: {
            filename: '[name].js',
            path: path.resolve('./tmp')
        },
        module: {
            rules: [
                {
                    test: /\.(jsx|js)$/,
                    use: {
                        loader: 'babel-loader'
                    }
                },
                {
                    test: /\.(css|sass|scss)$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: true,
                                    importLoaders: 1
                                }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: true,
                                    ident: 'postcss',
                                    plugins: () => [
                                        require('autoprefixer')(),
                                    ]
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: true
                                }
                            }
                        ]
                    })
                },
                {
                    test: /\.(png|gif|jpg|svg|ttf|eot|woff|woff2)$/,
                    use: {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]'
                        }
                    }
                },
                {
                    test: /\.(glsl|vs|fs)$/,
                    loader: 'shader-loader',
                    options: {
                        glsl: {
                            chunkPath: path.resolve('/glsl/chunks')
                        }
                    }
                }
            ],
        },
        plugins: [
            new ExtractTextPlugin({
                filename: '[name].css'
            }),
            // new CircularDependencyPlugin({
            //     // exclude detection of files based on a RegExp
            //     exclude: /a\.js|node_modules/,
            //     // add errors to webpack instead of warnings
            //     failOnError: true
            // })
        ],
        resolve: {
            extensions: ['.json', '.js', '.jsx']
        }
    };
};
